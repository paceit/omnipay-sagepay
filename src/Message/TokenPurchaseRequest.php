<?php

namespace Omnipay\SagePay\Message;

class TokenPurchaseRequest extends \Omnipay\SagePay\Message\ServerPurchaseRequest
{

    protected function getBaseData()
    {
        $token = $this->getToken();

        $data = parent::getBaseData();

        if ($token !== null) {
            $data['Token'] = $token;
        } else {
            $data['CreateToken'] = 1;
        }
        
        $data['StoreToken'] = 1;

        return $data;
    }
}
