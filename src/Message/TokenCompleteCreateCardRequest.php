<?php namespace Omnipay\SagePay\Message;

use Omnipay\Common\Exception\InvalidResponseException;

class TokenCompleteCreateCardRequest extends AbstractRequest
{
    public function getRequestTransactionId()
    {
        return $this->httpRequest->request->get('VendorTxCode');
    }

    public function getCardReference()
    {
        return isset($this->data['Token']) ? $this->data['Token'] : null;
    }

    public function getCardType()
    {
        return isset($this->data['CardType']) ? $this->data['CardType'] : null;
    }

    public function getLast4Digits()
    {
        return isset($this->data['Last4Digits']) ? $this->data['Last4Digits'] : null;
    }

    public function getExpiryDate()
    {
        return isset($this->data['ExpiryDate']) ? $this->data['ExpiryDate'] : null;
    }

    public function getData()
    {
        $this->validate('vendor', 'transactionReference');

        $reference = json_decode($this->getTransactionReference(), true);

        $signature = md5(
            $this->httpRequest->request->get('VPSTxId', '') .
            $this->httpRequest->request->get('VendorTxCode', '') .
            $this->httpRequest->request->get('Status', '') .
            strtolower($this->getVendor()) .
            $this->httpRequest->request->get('Token', '') .
            $reference['SecurityKey']

        );

        if (strtolower($this->httpRequest->request->get('VPSSignature')) !== $signature) {
            throw new InvalidResponseException;
        }

        return $this->httpRequest->request->all();
    }

    public function send()
    {
        return $this->response = new TokenCompleteCreateCardResponse($this, $this->getData());
    }
}