<?php namespace Omnipay\SagePay\Message;

class TokenCreateCardRequest extends ServerPurchaseRequest
{
    protected $action = 'TOKEN';

    public function getData()
    {
        $this->validate('returnUrl', 'transactionId');

        $data                    = $this->getBaseData();
        $data['VendorTxCode']    = $this->getTransactionId();
        $data['Currency']        = $this->getCurrency();
        $data['NotificationURL'] = $this->getReturnUrl();
        $data['Profile']         = $this->getProfile();

        return $data;
    }

    public function getService()
    {
        return 'token';
    }

    protected function createResponse($data)
    {
        return $this->response = new TokenCreateCardResponse($this, $data);
    }
}