<?php namespace Omnipay\SagePay\Message;

class TokenDeleteCardRequest extends ServerPurchaseRequest
{
    protected $action = 'REMOVETOKEN';

    public function getData()
    {
        $data          = $this->getBaseData();
        $data['Token'] = $this->getToken();

        return $data;
    }

    public function getService()
    {
        return 'removetoken';
    }

    protected function createResponse($data)
    {
        return $this->response = new Response($this, $data);
    }
}